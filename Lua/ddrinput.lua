require "gd"

--this order
--select, start, left, down, up, right, b, a
local order={8,7,4,5,6,3,9,10}
local image={}

local delay=120
local display=70
local sanity=2000

local period=240--for color; make it a multiple of 6

local merror=10

local holds=0

--detect holds by going forward, then draw the symbols backward
--this array is necessary
local backstring={}
local glowlevel={}

--read file as string
fle=io.open("minpress.fm2", "rb")
memstr=fle:read("*a")
fle:close()

--find beginning
i=1
for j=1,sanity,1 do
 if string.sub(memstr,i,i)=="|" then
  break
 end
 i=i+1 
end
baseoffset=i

local s1=[[
.xxxxxx.........
xxOOOOxx........
xOxxxxOx........
xOx..xxx........
xOxxxxx.........
xxOOOOxx........
.xxxxxOx........
xxx..xOxxxxxxxxx
xOxxxxOxOOOOOOOx
xxOOOOxxOxxxxxxx
.xxxxxxxOxxxxxxx
.......xOOOOOOOx
.......xOxxxxxxx
.......xOxxxxxxx
.......xOOOOOOOx
.......xxxxxxxxx
]]

local s2=[[
.xxxxxx.........
xxOOOOxx........
xOxxxxOx........
xOx..xxx........
xOxxxxx.........
xxOOOOxx........
.xxxxxOxxxxxxxxx
xxx..xOxOOOOOOOx
xOxxxxOxxxxOxxxx
xxOOOOxx..xOx...
.xxxxxx...xOx...
..........xOx...
..........xOx...
..........xOx...
..........xOx...
..........xxx...
]]

local s3=[[
................
......xxx.......
.....xxOx.......
....xxOOx.......
...xxOxOxxxxxxxx
..xxOxxOOOOOOOOx
.xxOxxxxxxxxxxOx
xxOxx........xOx
xOxx.........xOx
xxOxx........xOx
.xxOxxxxxxxxxxOx
..xxOxxOOOOOOOOx
...xxOxOxxxxxxxx
....xxOOx.......
.....xxOx.......
......xxx.......
]]

local s4=[[
...xxxxxxxxx....
...xOOOOOOOx....
...xOxxxxxOx....
...xOx...xOx....
...xOx...xOx....
...xOx...xOx....
...xOx...xOx....
xxxxOx...xOxxxx.
xOOOOx...xOOOOx.
xxOxxx...xxxOxx.
.xxOxx...xxOxx..
..xxOxx.xxOxx...
...xxOxxxOxx....
....xxOxOxx.....
.....xxOxx......
......xxx.......
]]


local s5=[[
......xxx.......
.....xxOxx......
....xxOxOxx.....
...xxOxxxOxx....
..xxOxx.xxOxx...
.xxOxx...xxOxx..
xxOxxx...xxxOxx.
xOOOOx...xOOOOx.
xxxxOx...xOxxxx.
...xOx...xOx....
...xOx...xOx....
...xOx...xOx....
...xOx...xOx....
...xOxxxxxOx....
...xOOOOOOOx....
...xxxxxxxxx....
]]

local s6=[[
................
.......xxx......
.......xOxx.....
.......xOOxx....
xxxxxxxxOxOxx...
xOOOOOOOOxxOxx..
xOxxxxxxxxxxOxx.
xOx........xxOxx
xOx.........xxOx
xOx........xxOxx
xOxxxxxxxxxxOxx.
xOOOOOOOOxxOxx..
xxxxxxxxOxOxx...
.......xOOxx....
.......xOxx.....
.......xxx......
]]

local s7=[[
.xxxxxxxxxxxx...
.xOOOOOOOOOOxx..
.xOxxxxxxxxxOxx.
.xOx.......xxOx.
.xOx........xOx.
.xOx.......xxOx.
.xOxxxxxxxxxOxx.
.xOOOOOOOOOOxx..
.xOxxxxxxxxxOxx.
.xOx.......xxOx.
.xOx........xOx.
.xOx.......xxOx.
.xOxxxxxxxxxOxx.
.xOOOOOOOOOOxx..
.xxxxxxxxxxxx...
................
]]

local s8=[[
......xxxx......
.....xxOOxx.....
.....xOxxOx.....
....xxOxxOxx....
....xOxxxxOx....
...xxOxxxxOxx...
...xOxx..xxOx...
..xxOxxxxxxOxx..
..xOOOOOOOOOOx..
.xxOxxxxxxxxOxx.
.xOxx......xxOx.
xxOx........xOxx
xOxx........xxOx
xOx..........xOx
xxx..........xxx
................
]]

image[1]=s1
image[2]=s2
image[3]=s3
image[4]=s4
image[5]=s5
image[6]=s6
image[7]=s7
image[8]=s8

function soverlay(str, x, y, col, ocol)
  for i=0, 15, 1 do
   for j=0, 15, 1 do
    chr=string.sub(str,17*i+j+1,17*i+j+1)
	if chr=="O" then
	 gui.pixel(x+j,y+i,col)
	end
	if chr=="x" then
	 gui.pixel(x+j,y+i,ocol)
	end
   end
  end
end

function retrievecolor(refframe)
 phase=refframe%period
 sixth=period/6
 i=math.floor(phase/sixth)
 part=phase%sixth
 frac=math.floor((256*part)/sixth)
 if i==0 then
  r0=255
  g0=frac
  b0=0
 elseif i==1 then
  r0=255-frac
  g0=255
  b0=0
 elseif i==2 then
  r0=0
  g0=255
  b0=frac
 elseif i==3 then
  r0=0
  g0=255-frac
  b0=255
 elseif i==4 then
  r0=frac
  g0=0
  b0=255
 elseif i==5 then
  r0=255
  g0=0
  b0=255-frac  
 end
 --g0=math.floor(g0*0.8)
 return {r=r0,g=g0,b=b0,a=255}
end

while false do

tabl=retrievecolor(emu.framecount())
gui.text(0,0,tabl.r)
gui.text(0,10,tabl.g)
gui.text(0,20,tabl.b)

emu.frameadvance()
end

while true do

 gui.box(0,0,255,255,"black")

 trueframe=emu.framecount()-delay

 for j=1,8,1 do
 
   for i=-merror,display,1 do
    merof=merror+i
    refframe=trueframe+i
      if refframe<0 then backstring[merof]=0
	   glowlevel[merof]=0
	  else
	   offset=baseoffset+refframe*23+order[j]  --size of frame is 23 bytes
	   if not string.byte(memstr,offset,offset) then backstring[merof]=0
             if i==-merror then glowlevel[merof]=0
	     else
	      glowlevel[merof]=glowlevel[merof-1]-1
	     end
	   else
	    chr=string.sub(memstr,offset,offset)
	    if chr=="." then backstring[merof]=0
		 if i==-merror then glowlevel[merof]=0
		 else
		  glowlevel[merof]=glowlevel[merof-1]-1
		 end
		else
         backstring[merof]=1
		 if i==-merror then glowlevel[merof]=1
		 else
		  glowlevel[merof]=10
--		  if glowlevel[merof-1]==10 then glowlevel[merof]=10
--		  else
--		   glowlevel[merof]=glowlevel[merof-1]+1
--		  end
		 end
		end
	   end
	  end
   end
   
   for i=display,0,-1 do
    merof=merror+i
    refframe=trueframe+i
    if backstring[merof]==1 then
	 if backstring[merof-1]==1 then
	  for k=0,2,1 do
	   soverlay(image[j],10+j*16,25+i*3-k,retrievecolor(refframe),"clear")
	  end
	 else
	   soverlay(image[j],10+j*16,25+i*3,retrievecolor(refframe),"black")
	 end
	end
   end
   
   mh=math.floor((256*glowlevel[10])/10)
   soverlay(image[j],10+j*16,25-1,"#C0C0C0FF",{r=mh,g=mh,b=mh,a=255})
   
   if backstring[10]==1 and backstring[9]==0 then
    holds=holds+1
   end

 end

local text=[[
meh
]]
local fontname = "arialuni.ttf" -- Common on Windows systems
gd.useFontConfig(false)

--holds=emu.framecount()

if holds<10 then
 holdstr="   "..holds
elseif holds<100 then
 holdstr="  "..holds
elseif holds<1000 then
 holdstr=" "..holds
else
 holdstr=""..holds
end

im = gd.createTrueColor(50, 20)
white = im:colorAllocate(255, 255, 255)
black = im:colorAllocate(0, 0, 0)
x, y = im:sizeXY()
im:filledRectangle(0, 0, x, y, black)
im:stringFT(white, fontname, 16, 0, 5, 17, holdstr)
gui.gdoverlay(60,3,im:gdStr())

emu.frameadvance()
end





while false do

 gui.box(0,0,255,255,"black")
 trueframe=emu.framecount()-delay
 
 for i=display,0,-1 do
    refframe=trueframe+i
	for j=1,8,1 do
	   if refframe<0 then break end
	   offset=baseoffset+refframe*23+order[j]
	   if not string.byte(memstr,offset,offset) then break end
	   chr=string.sub(memstr,offset,offset)
	   if chr~="." then
            soverlay(image[j],10+j*16,10+i*6,"white","black")
	   end
    end
 end
emu.frameadvance()
end


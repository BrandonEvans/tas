--Written by Brandon Evans

local addresses = {
	bugs = {
		x = 0x1804,
		x_screen = 0x0001,
		y = 0x1806,
		y_screen = 0x0000
	},
	camera = {
		x = 0x0382
	}
}

local x
local y
local x_screen
local y_screen
local prev_x = 0
local prev_y = 0

function update()
	--Update the data for the addresses.
	x = mainmemory.read_u8(addresses.bugs.x) +
		(mainmemory.read_u8(addresses.bugs.x + 1) * 256)
	y = mainmemory.read_u8(addresses.bugs.y)
	x_screen = mainmemory.read_u8(addresses.bugs.x_screen)
	y_screen = mainmemory.read_u8(addresses.bugs.y_screen)
end

function frame(show_x, show_y)
	--Display the stats and frame advance.
	gui.text(x_screen - 30, y_screen - 40, show_x .. ', ' .. show_y)
	gui.text(x_screen - 30, y_screen + 10, (show_x - prev_x) .. ', ' .. (show_y - prev_y), 'Green')
	prev_x = show_x
	prev_y = show_y
	emu.frameadvance()
end

while true do
	update()
	local x_show = x
	local y_show = y
	if y_screen == 72 then
		gui.text(0, 128, 'In Doors')
		x_show = x_screen
		--Get the key.
		joypad.set({Right=true})
		if x_screen == 76 then
			--Prepare to turn around.
			for i = 1, 4 do
				update()
				joypad.set({Right = true})
				frame(x_show, y_show)
			end
			--Turn around.
			for i = 1, 4 do
				update()
				joypad.set({Left = true})
				frame(x_show, y_show)
			end
			--Exit through the door.
			while y_screen == 72 do
				update()
				joypad.set({Up = true})
				frame(x_show, y_show)
			end
		end
		joypad.set({})
	end
	frame(x_show, y_show)
end
--7E00EX: Enemies Positions
while true do
    local x = memory.readbyte(0x7E00D1) + (memory.readbyte(0x7E00D2) * 256)
    gui.text(0, 0, x)
    local c = {right=true, Y=true}
    c.A = x > 1242 and x < 1300
    joypad.set(1, c)
    emu.frameadvance()
end
--Partycated NitrodonE
local horizontal = true --TODO: Attempt to automate.
local toggled = false

local cam = {
    curr={}, prev={}, data = {
        major=0x047C, minorx=0x00FD, minory=0x00FC
    }
}
local characters = {
    {
        --Willy
        data = {
            health=0x0423, screenx=0x0434, screeny=0x0435, subx=0x043D,
            suby=0x043F
        }
    }
}
local majory = {curr=0, prev=0}
local states = {}

function get(obj, state)
    --Get a character's real positions based on the camera position.
    local x = obj[state].screenx
    local y = sign(obj[state].screeny, 224)
    --Add the camera position.
    if horizontal then
        x = x + cam[state].minorx + (cam[state].major * 256)
    else
        y = y + cam[state].minory + majory[state] * 240
    end
    --Add the subpixels when provided.
    if obj[state].subx then
        x = x + obj[state].subx * 1.0 / 0x100
    end
    if obj[state].suby then
        y = y + obj[state].suby * 1.0 / 0x100
    end
    return {x=x, y=y}
end

function label(obj)
    --Label a character's x, y, and health.
    load(obj)
    local curr = obj.curr
    local prev = obj.prev
    if curr.screeny == 240 and 240 - curr.screenx < 2 then
        return
    end
    local currpos = get(obj, 'curr')
    local prevpos = get(obj, 'prev')
    local signedy = sign(curr.screeny, 224)
    gui.text(curr.screenx - 16, signedy + 36, '       ', 'white', 'black')
    gui.text(
        curr.screenx - 16, signedy + 36, currpos.x .. ', ', 'white', 'black'
    )
    gui.text(curr.screenx + 24, signedy + 36, currpos.y, 'white', 'black')
    gui.text(curr.screenx - 16, signedy + 46, '       ', 'white', 'black')
    gui.text(
        curr.screenx - 16,
        signedy + 46,
        currpos.x - prevpos.x .. ', ',
        'green',
        'black'
    )
    gui.text(
        curr.screenx + 24,
        signedy + 46,
        currpos.y - prevpos.y,
        'green',
        'black'
    )
    if curr.health then
        gui.text(curr.screenx + 4, signedy - 10, curr.health, 'red', 'black')
    end
end

function load(obj)
    --Load and cache data from the provided addresses.
    for index, value in pairs(obj.data) do
        --Cache the current data for the object if available.
        obj.prev[index] = obj.curr[index]
        if obj.prev[index] == nil then
            --Get new ones if not.
            obj.prev[index] = memory.readbyte(value)
        end
        --Load the new data for the object.
        obj.curr[index] = memory.readbyte(value)
    end
end

function sign(value, maximum)
    --Artificially sign a value after it goes past a maximum.
    if value >= maximum then
        value = -(256 - value)
    end
    return value
end

function iff(cond, a, b)
    --If and only if.
    if cond then
        return a
    else
        return b
    end
end

for offset = 0x0, 0x2 do
    table.insert(
        characters,
        {
            data = {
                health=0x04B9 + offset,
                screenx=0x04CB + (offset * 0x2),
                screeny=0x04D1 + (offset * 0x2),
                subx=0x04E9 + offset,
                suby=0x04EC + offset
            }
        }
    )
end

for index, value in pairs(characters) do
    characters[index].curr = {}
    characters[index].prev = {}
end

while true do
    load(cam)
    if input.get().control and not toggled then
        horizontal = not horizontal
        if not horizontal then
            --Default the artificial majory to the most relevant address.
            majory.curr = memory.readbyte(0x047C)
        end
    end
    if not horizontal then
        --Define the major y by tracking overflows / underflows.
        if math.abs(cam.curr.minory - cam.prev.minory) > 220 then
            majory.curr = majory.curr + iff(
                cam.curr.minory - cam.prev.minory > 0, -1, 1
            )
        end
        --Reset when the room resets. TODO: Control this with a new room flag.
        if math.abs(cam.curr.major - cam.prev.major) > 1 then
            majory.curr = 0
        end
    end
    for index, value in pairs(characters) do
        label(value)
    end
    majory.prev = majory.curr
    if memory.readbyte(0x04E0) > 0 then
        gui.text(0, 10, 'Invul: ' .. memory.readbyte(0x04E0), 'green', 'black')
    end
    --Label the position of the punch glove when it is out.
    if memory.readbyte(0x0443) ~= 0 then
        local glove = {
            curr = {
                screenx=memory.readbyte(0x0437),
                screeny=memory.readbyte(0x0438)
            }
        }
        local willy = characters[1].curr
        local pos = get(glove, 'curr')
        local signedy = sign(willy.screeny, 224)
        gui.text(willy.screenx - 16, signedy + 56, '       ', 'red', 'black')
        gui.text(
            willy.screenx - 16, signedy + 56, pos.x .. ', ', 'red', 'black'
        )
        gui.text(willy.screenx + 24, signedy + 56, pos.y, 'red', 'black')
    end
    --Prevent the toggling from occuring multiple times while holding control.
    toggled = input.get().control
    gui.text(0, 20, memory.readbyte(0x00FC))
    gui.text(0, 30, memory.readbyte(0x047C))
    emu.frameadvance()
end
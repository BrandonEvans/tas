local xnew = 0
local ynew = 0
local lost = 0

while true do
    local mover = memory.readbyte(0x512)
    local x = memory.readbyte(0x501) + (256 * memory.readbyte(0x502))
    local y = memory.readbyte(0x503)
    if x == xnew and y == ynew and mover == 0 then
        lost = lost + 1
    end
    xnew = x
    ynew = y
    gui.text(10,10,"Lost frames: " .. lost)
    emu.frameadvance()
end
local initiated = false
local button = 'B'

function run(controller, messages)
	local data = {buttons={}, messages={}}
	if not initiated then
		data.display = 'Initiated playing!'
		initiated = true
	end
	--Alternate pressing buttons.
	if button == 'A' then
		button = 'B'
	else
		button = 'A'
	end
	data.buttons[button] = true
	--If this frame is divisible by 1000, press start and tell bot 2
	--not to press anything.
	if emu.framecount() % 1000 == 0 then
		data.display = '1000 frames have passed. Pressing start!'
		data.buttons.start = true
		data.messages[2] = {release=true}
	end
	return data
end
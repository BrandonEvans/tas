while true do
    local x = memory.readbyte(0x0086) + (memory.readbyte(0x006D) * 256)
    gui.text(0, 8, x)
    for i = 0, 4 do
        local enemy = memory.readbyte(0x000F + i)
        local enemyx = memory.readbyte(0x0087 + i) + (
            memory.readbyte(0x006E + i) * 256
        )
        local enemyluck = memory.readbyte(0x003C + i)
        if enemy == 1 then
            gui.text(
                0, 8 * (i + 2),
                tostring(enemyx) .. ' - ' .. tostring(enemyluck)
            )
        end
    end
    emu.frameadvance()
end
release = 0

function run(controller, messages)
	local data = {buttons={A=true}}
	if messages[1] ~= nil and messages[1].release == true then
		data.display = 'Bot 2 told me to release A for 100 frames.'
		release = 100
	end
	if release > 0 then
		data.buttons.A = false
		release = release - 1
	end
	return data
end
while true do
    local x = memory.readbyte(0x7E0219) + (memory.readbyte(0x7E0078) * 256)
    local y = memory.readbyte(0x7E0F9F)
    gui.text(0, 30, 'x: ' .. x)
    gui.text(0, 40, 'y: ' .. y)
    emu.frameadvance()
end
--Spider-Man & X-Men: Arcade's Revenge
--Written by Brandon Evans for a TAS made with jaysmad
--Copyright � 2011-2012 Brandon Evans
local character = 1
local mode = 1

local juggernaut = false
local toggledl = false
local toggledr = false

local cameras = {
    --Mode 1: Spider-Man.
    {
        bytes={},
        words = {
            x=0x7E005A, y=0x7E005E
        }
    },
    --Mode 2: Wolverine, Cyclops, and underwater Storm.
    {
        bytes={},
        words = {
            x=0x7E002E, y=0x7E0030
        }
    },
    --Mode 3: Gambit and walking Storm.
    {
        bytes={},
        words = {
            x=0x7E0074, y=0x7E0076
        }
    },
    --Mode 4: Arcade.
    {
        bytes={
            x=0x7E0058, y=0x7E005C
        },
        words = {}
    }
}
local enemies = {}
local objects = {
    --Characters 1: Spider-Man.
    {
        {
            bytes = {
                health=0x7E10F8
            },
            words = {
                x=0x7E0628,
                y=0x7E062A
            }
        }
    },
    --Characters 2: Wolverine, Cyclops, and Underwater Storm.
    {
        {
            bytes = {
                health=0x7E0B29
            },
            words = {
                x=0x7E0762,
                y=0x7E07EC
            }
        }
    },
    --Characters 3: Gambit and Walking Storm.
    {
        {
            bytes = {
                health=0x7E119E
            },
            words = {
                x=0x7E0630,
                y=0x7E0632
            }
        }
    }
}
local levels = {
    {
        bytes={},
        label='Water',
        words = {
            y=0x7E14DC
        }
    },
    {
        bytes={},
        label='Ball / Platform',
        words = {
            y=0x7E11FA
        }
    }
}

function default(obj)
    --Set an object to its defaults.
    obj.changes = {}
    obj.curr = {}
    obj.prev = {}
end

function display(obj)
    --Display the data for a given character.
    load(obj)
    local cam = cameras[mode].curr
    local curr = obj.curr
    local prev = obj.prev
    --If this type of object shouldn't be labeled, return.
    if curr.id and not enemies[character][curr.id].name then
        return
    end
    --If the addresses presumably have been reused based on whether or not the 
    --distance between the object and its previous position is massive, treat
    --it as if it were dead.
    if (
        math.abs(curr.x - prev.x) > 100 or
        math.abs(curr.y - prev.y) > 100
    ) then
        default(obj)
        return
    end
    --Offset the text positions depending on the mode.
    local camx = cam.x
    local camy = cam.y
    local x = -36
    local y = -36
    if mode ~= 2 then
        x = 0
        y = 0
    end
    if mode == 1 then
        camx = camx * 2
        camy = camy * 2
    end
    --Mark a change in the character's health if there is one.
    local comparison = curr.health - prev.health
    if comparison ~= 0 then
        table.insert(
            obj.changes,
            {changes=comparison, distance=0, time=0}
        )
    end
    local base = curr.x - camx + x
    if juggernaut and obj.bytes.health == 0x7E0B4F and base - 15 < 0 then
        base = 15
    end
    if mode == 3 and (
        obj.bytes.health ~= 0x7E119E and obj.bytes.health ~= 0x7E0652 and
        obj.bytes.health ~= 0x7E067C and obj.bytes.health ~= 0x7E06A6
    ) then
        return
    end
    --Display the object's stats relative to its position.
    gui.text(
        base - 15,
        curr.y - camy + y,
        tostring(curr.x) .. ','
    )
    gui.text(
        base + 10,
        curr.y - camy + y,
        tostring(curr.y)
    )
    gui.text(
        base - 15,
        curr.y - camy + y + 10,
        tostring(curr.x - prev.x) .. ',',
        'green'
    )
    gui.text(
        base + 10,
        curr.y - camy + y + 10,
        tostring(curr.y - prev.y),
        'green'
    )
    gui.text(
        base - 5,
        curr.y - camy + y - 50,
        curr.health,
        'red'
    )
    if curr.id and enemies[character][curr.id].name then
        gui.text(
            base - 15,
            curr.y - camy - 60 + y,
            enemies[character][curr.id].name,
            'red'
        )
    end
    --Display the character's changes in health.
    for index, value in pairs(obj.changes) do
        --Have the changes go up 0.5 pixels a frame.
        local distance = 1 * (value.time % 2)
        value.distance = value.distance - distance
        --Stop after traveling 30 pixels.
        if value.distance < -30 then
            value.distance = -30
        end
        gui.text(
            base - 5,
            curr.y - camy - 60 + value.distance + y,
            value.changes,
            'red'
        )
        value.time = value.time + 1
        --Stop displaying after a second and a half.
        if value.time == 90 then
            table.remove(obj.changes, index)
        end
    end
end

function load(obj)
    --Load and cache bytes and words from the provided addresses.
    for index, value in pairs(obj.bytes) do
        --Cache the current bytes for the object if available.
        obj.prev[index] = obj.curr[index]
        if obj.prev[index] == nil then
            --Get new ones if not.
            obj.prev[index] = memory.readbyte(value)
        end
        --Load the new bytes for the object.
        obj.curr[index] = memory.readbyte(value)
    end
    for index, value in pairs(obj.words) do
        --Cache the current words for the object if available.
        obj.prev[index] = obj.curr[index]
        if obj.prev[index] == nil then
            --Get new ones if not.
            obj.prev[index] = memory.readword(value)
        end
        --Load the new words for the object.
        obj.curr[index] = memory.readword(value)
    end
end

function reset()
    --Reset all of the objects.
    for index, value in pairs(
        {cameras, objects[1], objects[2], objects[3], levels}
    ) do
        for index2, obj in pairs(value) do
            default(obj)
        end
    end
end

--Label all of the objects.
for offset = 0x0, 0x88 do
    --Mode 1 / 4.
    table.insert(
        objects[1],
        {
            bytes = {
                id=0x7E06F4 + (offset * 0x34),
                health=0x7E070A + (offset * 0x34)
            },
            words = {
                x=0x7E06F8 + (offset * 0x34),
                y=0x7E06FA + (offset * 0x34)
            }
        }
    )
    --Mode 2.
    table.insert(
        objects[2],
        {
            bytes = {
                id=0x7E087A + (offset * 0x2),
                health=0x7E0B2D + (offset * 0x2)
            },
            words = {
                x=0x7E0766 + (offset * 0x2),
                y=0x7E07F0 + (offset * 0x2)
            }
        }
    )
    --Mode 3.
    table.insert(
        objects[3],
        {
            bytes = {
                health=0x7E0652 + (offset * 0x6)
            },
            words = {
                x=0x7E065A + (offset * 0x6),
                y=0x7E065C + (offset * 0x6)
            }
        }
    )
end

for i = 1, 3 do
    enemies[i] = {}
    for j = 0, 255 do
        enemies[i][j] = {}
    end
end

enemies[1][6].name = 'Thrower'
enemies[1][10].name = 'Grey Wall Crawler'
enemies[1][12].name = 'Grey Hover'
enemies[1][14].name = 'Shocker'
enemies[1][16].name = 'Orange Wall Crawler'
enemies[1][18].name = 'Red Hover'
enemies[1][22].name = 'N\'astirh'
enemies[1][26].name = 'Ceiling Crawler'
enemies[1][28].name = 'Carnage'
enemies[1][32].name = 'Rhino'
enemies[1][36].name = 'Arcade Machine'
enemies[1][38].name = 'Arcade'
enemies[1][40].name = 'X-Men'
enemies[2][16].name = 'Blue Shooter'
enemies[2][24].name = 'Jack-in-the-Box'
enemies[2][28].name = 'Soldier'
enemies[2][32].name = 'Clown'
enemies[2][40].name = 'Squid'
enemies[2][48].name = 'Plane'
enemies[2][64].name = 'Clam'
enemies[2][80].name = 'Pilot'

reset()
savestate.registerload(reset)

while true do
    if input.get().leftbracket and not toggledl then
        mode = mode + 1
        if mode > 4 then
            mode = 1
        end
    end
    character = mode
    if mode == 4 then
        character = 1
    end
    if input.get().rightbracket and not toggledr then
        juggernaut = not juggernaut
    end
    load(cameras[mode])
    for index, value in pairs(objects[character]) do
        display(value)
    end
    --Hack the camera for the Dark Queen fight.
    if (
        mode == 3 and objects[character][1].curr.y ~= nil and
        objects[character][1].curr.y < 232
    ) then
        memory.writeword(cameras[mode].words.y, 0)
    end
    gui.text(0, 30, 'Mode: ' .. mode)
    gui.text(0, 40, 'Juggernaut: ' .. tostring(juggernaut))
    if character ~= 1 then
        local obj = levels[mode - 1]
        load(obj)
        gui.text(0, 50, obj.label .. ': ' .. obj.curr.y)
        gui.text(
            0, 60, obj.label .. ' Speed: ' .. obj.curr.y - obj.prev.y
        )
    end
    --Prevent the toggling from occuring multiple times while holding buttons.
    toggledl = input.get().leftbracket
    toggledr = input.get().rightbracket
    emu.frameadvance()
end
--You can adjust the base locations for text here.
local x = 0
local y = 30
--You can adjust the display timer length here.
local length = 200

local bots = {}
local display = {}
local messages = {}
local running = 0

function requireBot()
	require("bot" .. (table.getn(bots) + 1))
end

while pcall(requireBot) do
	local func = false
	--The bot isn't running if a function isn't provided.
	if run ~= nil then
		running = running + 1
		func = run
	end
	table.insert(bots, func)
	table.insert(display, {text='', timer=0})
	table.insert(messages, {})
end

while true do
	local send = {}
	gui.text(x, y, 'Bots running: ' .. running)
	for controller, bot in pairs(bots) do
		--Work with the bot only if it is running.
		if bot then
			local data = bot(controller, messages[controller])
			if data ~= nil then
				--If buttons are provided, press them for this bot's
				--controller.
				if data.buttons ~= nil then
					joypad.set(controller, data.buttons)
				end
				--If text to display is provided, store it and start
				--the timer.
				if data.display ~= nil then
					display[controller].text = data.display
					display[controller].timer = length
				end
				--If messages are provided, prepare to send them.
				if data.messages ~= nil then
					send[controller] = data.messages
				end
			end
			--If the timer is still running, display the text and
			--decrement the timer.
			if display[controller].timer > 0 then
				gui.text(
					x, y + (controller * 10),
					'Bot ' .. controller .. ': ' ..
					display[controller].text
				)
				display[controller].timer = display[
					controller
				].timer - 1
			end
		end
		messages[controller] = {}
	end
	for sender, outbox in pairs(send) do
		for receiver, message in pairs(outbox) do
			messages[receiver][sender] = message
		end
	end
	emu.frameadvance()
end
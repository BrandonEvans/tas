local exited = false
local room = memory.readbyte(0x0154)
local x
local y

while true do
    local key = {}
    local text = memory.readbyte(0x506)
    if text > 36 then
        key.A = 1
    end
    joypad.set(1, key)

    local newx = memory.readbyte(0x0032) + (memory.readbyte(0x0032) * 256)
    local newy = memory.readbyte(0x0034) + (memory.readbyte(0x0035) * 256)
    newx = (newx - 8) / 16
    newy = (newy - 8) / 16
    if x == nil then
        x = newx
        y = newy
    end
    gui.text(0, 8, '          ')
    gui.text(0, 8, newx .. ', ')
    gui.text(60, 8, newy)
    gui.text(0, 16, '          ')
    gui.text(0, 16, (newx - x) .. ', ', 'green')
    gui.text(60, 16, newy - y, 'green')
    x = newx
    y = newy

    local newroom = memory.readbyte(0x0154)
    if room ~= newroom then
        exited = movie.framecount()
    end
    if exited ~= false then
        gui.text(0, 24, 'Exited: ' .. exited)
        if movie.framecount() == exited + 120 then
            exited = false
        end
    end
    room = newroom

    local sub = memory.readbyte(0x7093)
    gui.text(165, 8, 'Subpixel value: ' .. sub)
    if sub < 20 then
       gui.text(165, 16, 'Turn now!')
    end
    FCEU.frameadvance()
end
1
00:00:02,000 --> 00:00:05,000
Thanks for watching this TAS of Spider-Man/X-Men: Arcade's Revenge by jaysmad and Brandon Evans.

2
00:00:10,136 --> 00:00:13,136
jaysmad primarily authored the Spider-Man sections.

3
00:00:15,760 --> 00:00:18,760
Taking damage here is necessary to save time.

4
00:00:21,234 --> 00:00:24,234
For those who've never played this game, you need to deactivate the bombs in a specific order.

5
00:00:33,864 --> 00:00:36,864
When hit, we are propelled in the opposite direction. This allows us to take many shortcuts.

6
00:00:48,107 --> 00:00:51,107
Web-swinging and web-cancel are much faster than walking or jumping.

7
00:00:56,643 --> 00:00:59,643
As web-swinging makes you skip a pixel, we are able to damage boost here.

8
00:01:14,330 --> 00:01:17,330
Turning around is very slow after web-canceling, so swinging is unnecessary here.

9
00:01:56,727 --> 00:01:59,727
We must wait until the web is totally gone until we can swing again.

10
00:02:13,649 --> 00:02:16,649
We skip the Shockers as killing them is useless.

11
00:02:38,991 --> 00:02:41,991
While standing, we can only shoot once every 8 frames.

12
00:02:42,319 --> 00:02:45,319
We use the backward web-shot trick, which consists of dropping down a ledge for 1 frame while shooting in the opposite direction.

13
00:02:45,647 --> 00:02:48,647
This technique allows us to shoot every 2 frames. Swinging does not hurt this boss and web-shots deal 1 damage.

14
00:02:48,975 --> 00:02:51,975
Considering that N'astirh has 30HP and each web-shot does 1 point of damage, this leads to him dying nearly instantly.

15
00:03:04,649 --> 00:03:07,649
We backtrack to reach the lower ledge for a big swing.

16
00:03:15,215 --> 00:03:18,215
We skip the higher section by swinging under the wall. This is the only known time where this is possible.

17
00:03:25,997 --> 00:03:28,997
Jumping against the wind will slow us down, so the best thing to do is either swing or take damage.

18
00:03:30,506 --> 00:03:33,506
Rhino has 24HP and can't be damaged by web-shots.

19
00:03:33,834 --> 00:03:36,834
Carnage has 25HP. When hit, he gets knocked back and becomes temporarily invincible.

20
00:03:37,162 --> 00:03:40,162
He receives 1 damage point from web-shots; he also takes 2 from a swing hit, but this can be doubled if positioned correctly.

21
00:03:49,475 --> 00:03:52,475
Brandon primarily authored the Wolverine sections.

22
00:03:57,545 --> 00:04:00,545
If properly timed, you can pass through a stunned Jack-in-the-Box unharmed.

23
00:04:01,239 --> 00:04:04,239
We punch at arbitrary times to manipulate the positions of certain enemies, namely the soldiers.

24
00:04:04,567 --> 00:04:07,567
This might work because this changes what addresses enemies are loaded into, as punching is perhaps handled like an enemy.

25
00:04:08,277 --> 00:04:11,277
If you withdraw your claws at exactly the right time, you can do the same damage as claws while dropping a heart.

26
00:04:12,188 --> 00:04:15,188
The Jack-in-the-Box normally drops hearts when you kill them with your fists. Collecting hearts is just for style.

27
00:04:15,382 --> 00:04:18,382
You can "kill" the water gun shots by hitting them. This becomes useful later.

28
00:04:19,892 --> 00:04:22,892
Most of this level has a fairly straightforward strategy. Just run and jump at the right times.

29
00:04:24,667 --> 00:04:27,667
You can slash in mid-air to take off 1 layer of the destructible wall.

30
00:04:28,977 --> 00:04:31,977
The punching manipulation allows us to hit these soldiers without stopping.

31
00:04:32,304 --> 00:04:35,304
We're not sure if the water gun objects can be manipulated, but doing so probably wouldn't help.

32
00:04:36,714 --> 00:04:39,714
This is the only known case in which we can detonate a block while simultaneously jumping on it.

33
00:04:41,772 --> 00:04:44,772
The punching manipulation also pays off here.

34
00:04:49,559 --> 00:04:52,559
Sadly, it doesn't seem like we can manipulate the clown.

35
00:05:16,166 --> 00:05:18,478
Jumping from here is fastest.

36
00:05:18,495 --> 00:05:21,495
Apocalypse has 48HP.

37
00:05:21,823 --> 00:05:24,823
Claws will hit a variable number of times per slash, doing 2 damage for each hit.

38
00:05:25,151 --> 00:05:28,151
We have tried multiple times and can't seem to do any better on this fight.

39
00:05:31,174 --> 00:05:34,174
WARNING: These subtitles contain spoilers. We recommend you watch this level without them first.

40
00:05:37,164 --> 00:05:40,164
Juggernaut begins with 32HP.

41
00:05:42,822 --> 00:05:45,822
Hitting him on the head with an anvil does 4 damage, while the 1 ton weights do 2 damage.

42
00:05:47,813 --> 00:05:50,813
Claws do 1 damage, but contrary to common belief, they will only do so at the end of the level.

43
00:05:52,805 --> 00:05:55,805
Worse yet, his health can't go below 2HP until you can use your claws.

44
00:05:57,797 --> 00:06:00,797
The game's failure to explain this has haunted non-assisted players!

45
00:06:01,824 --> 00:06:04,824
As Juggernaut gets pushed back whenever he collides with a block, we detonate the blocks in his way.

46
00:06:06,516 --> 00:06:09,516
From this point on, we try to get to the end as fast as we can purely for stylistic reasons.

47
00:06:09,844 --> 00:06:12,844
When Juggernaut collides with an anvil / weight, it does 1 damage, but this is useless and slows him down.

48
00:06:44,054 --> 00:06:47,054
...Does anything seem wrong with this picture?

49
00:06:54,454 --> 00:06:57,454
As we got Juggernaut down to 2HP, we merely need to perform 2 slashes, which are now both allowed.

50
00:06:59,545 --> 00:07:02,545
Although we're told that this can't be done on a console, this ending amuses us.

51
00:07:02,757 --> 00:07:05,757
jaysmad primarily authored the Cyclops sections.

52
00:07:15,586 --> 00:07:18,586
Rubies give life and points. When your HP is low, you lose the chance to shoot rapidly.

53
00:07:48,481 --> 00:07:51,481
The lower route is much slower than the higher route here.

54
00:08:15,470 --> 00:08:18,470
We take damage for entertainment purposes and to show that detonating a mine while in a cart doesn't automatically kill you.

55
00:08:25,354 --> 00:08:28,354
Jumping from this platform helps us avoid invisible blocks in the ceilling.

56
00:08:29,664 --> 00:08:32,664
Carts filled with rubies give large amounts of life and points.

57
00:08:47,185 --> 00:08:50,185
Running in a cart is not possible unless you enter the cart while running.

58
00:08:53,757 --> 00:08:56,757
The yellow enemies are immune to optic blasts, but the rocks that they throw at you aren't.

59
00:08:57,085 --> 00:09:00,085
Luckily, we can shoot the rock and punch the yellow enemy in a single jump.

60
00:09:32,627 --> 00:09:35,627
Big rubies yield a lot of HP, but more importantly, they double the power of Cyclops' beam.

61
00:09:35,954 --> 00:09:38,954
The boost wears off after shooting a couple of times, so it is best to save some shots for the boss.

62
00:09:44,524 --> 00:09:47,524
The sentinel has 127HP. The first shot deals 24 damage and 54 when boosted. After that, it will only deal 2 damage.

63
00:09:47,851 --> 00:09:50,851
The following shots must be done right after the sentinel lifts his arm.

64
00:10:05,572 --> 00:10:08,572
We stun the bot so he can't shoot, then stall for 2 frames to shoot the rock. This is much faster than jumping into the ceiling.

65
00:10:08,900 --> 00:10:11,900
As we can't get both the ruby and kill the bot, we choose the bot has it gives 600 points as opposed to 300.

66
00:10:27,270 --> 00:10:30,270
Here is another example of where stalling 2 frames to shoot is faster than jumping and punching.

67
00:10:37,869 --> 00:10:40,869
When the sentinel is up in the air, shooting only deals 2 damage until he lifts his arm.

68
00:11:00,449 --> 00:11:03,449
Master Mold is defeated in 3 parts: the arm, then the head, and then the body. Shooting deals 6 damage while hitting deals 16.

69
00:11:03,776 --> 00:11:06,776
The arm has 72HP. When he lifts his arm, it becomes out of reach so that it can only be shot.

70
00:11:07,104 --> 00:11:10,104
The head has 80HP and is out of reach, so you can only defeat it with shots.

71
00:11:11,963 --> 00:11:14,963
The body has 112HP and is out of range unless Master Mold is hovering on the left side of the screen.

72
00:11:30,482 --> 00:11:33,482
jaysmad primarily authored the Storm sections, which he blasted through despite being one of the slowest parts of the game.

73
00:11:35,474 --> 00:11:38,474
Storm's levels consist of blowing up water reactors to raise the water level in order to reach the end of the stage.

74
00:11:38,802 --> 00:11:40,865
It is impossible to reach the next reactor before the water stops raising, so there's no rush to blow them up.

75
00:11:40,882 --> 00:11:43,882
Shells give a lot of points and contain a pearl giving the ability to shoot a whirlwind that deflects enemy projectiles.

76
00:11:44,210 --> 00:11:47,210
Passing through the exploding doors is faster than waiting.

77
00:11:48,619 --> 00:11:51,619
8-Way Lightning allows Storm to do a shot going in every direction as well as destroy doors in 1 shot.

78
00:11:55,491 --> 00:11:58,491
Bubbles recover your air.

79
00:13:42,798 --> 00:13:45,798
Staying away from the exploding reactor and the fish reduces lag.

80
00:13:53,996 --> 00:13:56,996
You can stand in the electric current without taking damage.

81
00:14:51,069 --> 00:14:54,069
The boss is 10 glass globes that can only be hit 1 after the other.

82
00:14:54,397 --> 00:14:57,397
Each globe needs to be hit twice. After 24 frames, the next globe can be hit.

83
00:14:57,725 --> 00:15:00,725
The trick is that as soon as the next globe becomes vulnerable, there is a small amount of time in which 3 globes can be hit.

84
00:15:04,247 --> 00:15:07,247
After defeating the boss, it is impossible to take damage.

85
00:15:07,575 --> 00:15:10,575
There is 1 last reactor that raises the water level to the boss, but with the 8-Way Lightning, it becomes unnecessary.

86
00:15:10,903 --> 00:15:13,903
Still, we blow it up after the battle for extra points.

87
00:15:16,693 --> 00:15:19,693
Brandon primarily authored the Gambit sections.

88
00:15:20,021 --> 00:15:23,021
Gambit can throw cards forward, upward, and downward if he's in the air.

89
00:15:23,349 --> 00:15:26,349
The cards you throw forward go farther the longer you hold down the throwing button, and it's very precise. Perfect for TAS!

90
00:15:27,010 --> 00:15:30,010
Notice how we make the knight piece run into the death dealing card.

91
00:15:31,153 --> 00:15:34,153
You stop traveling horizontally for 1 frame when leaving the ground, so we avoid doing so whenever possible.

92
00:15:47,892 --> 00:15:34,564
The big debate for the next section, which involves a wall of blocks, is whether or not to use the fireball move.

93
00:15:34,581 --> 00:15:37,581
Early tests of it created lag and melted the block we need to jump off of, but we thought it might destroy the wall faster.

94
00:15:37,908 --> 00:15:40,908
We managed to make it not lag and melt the block below Gambit early enough to allow him to jump off of the block below it.

95
00:15:41,236 --> 00:15:44,236
This maneuver allowed us to get past the wall 2 frames faster than our best attempt without the move.

96
00:15:48,757 --> 00:15:51,757
Gambit hits his head to fall faster, then hugs the wall on the next jump. We'd lose time getting the item.

97
00:16:01,619 --> 00:16:04,619
Jumping here is faster than walking off. We absorb a bullet with the fireball move to avoid getting hurt.

98
00:16:14,648 --> 00:16:17,648
The King of Spades has 56HP and can be attacked with 8 cards, each dealing 1HP, per phase.

99
00:16:17,976 --> 00:16:20,976
After hit 8 times, he becomes invincible and starts circling you.

100
00:16:21,304 --> 00:16:24,304
Gambit's positioning when he starts and finishes moving greatly impacts how long it takes for him to stop.

101
00:16:24,631 --> 00:16:27,631
When he stops, he spawns enemies and eventually becomes vulnerable again. This cycle repeats until the end.

102
00:17:00,223 --> 00:17:03,223
In the final phase, we throw extra cards to occupy many of the addresses that the boss uses to spawn enemies.

103
00:17:03,551 --> 00:17:06,551
If we don't do this, there will be too many objects on the screen to throw all 8 cards.

104
00:17:06,879 --> 00:17:09,879
Another suicidal victory!

105
00:17:10,290 --> 00:17:13,290
Although this level is an autoscroller, it proved to be very entertaining thanks to Gambit's highly exploitable abilities.

106
00:17:21,271 --> 00:17:24,271
That's right! Gambit's cards can be used to make drum noises (Brandon plays the drum kit, by the way)!

107
00:17:25,664 --> 00:17:28,664
Some headbanging of sorts.

108
00:17:40,789 --> 00:17:43,789
You have to appreciate how well the explosion sound effect complements the bass drum hits.

109
00:17:50,240 --> 00:17:53,240
Headbanging in the air, which looks a little more realistic.

110
00:18:07,246 --> 00:18:10,246
Here's an example of one of the many invisible platforms found in this level.

111
00:18:11,023 --> 00:18:14,023
Jumping and starting to move after starting to throw a card up makes Gambit's running animation play in the air.

112
00:18:16,314 --> 00:18:19,314
More sliding like we had in the previous level's boss fight.

113
00:18:29,925 --> 00:18:32,925
It wouldn't be an autoscroller if we didn't have to resort to doing left-right-left-right a lot. Notice the headbanging?

114
00:18:52,604 --> 00:18:55,604
In order to collect the maximum number of items in this section, we have to move faster than the camera.

115
00:19:01,107 --> 00:19:04,107
The column of items farthest towards the right doesn't seem to be collectible for some reason.

116
00:19:07,730 --> 00:19:10,730
If you collide with the wall at a certain angle, you can pass through it, though we cannot go far enough to collect the stars inside.

117
00:19:11,057 --> 00:19:14,057
Doing so traps us inside the wall, and the only way out is by blowing up the block next to you, and it's too late to do that.

118
00:19:16,232 --> 00:19:19,232
Hooray for explosions!

119
00:19:21,207 --> 00:19:24,207
When Gambit throws cards off to the right side of the screen, pay attention to the left.

120
00:19:26,249 --> 00:19:29,249
Somehow, we managed to corrupt that tile repeatedly...

121
00:19:29,577 --> 00:19:32,577
The cards manage to blow up blocks on the other side of the screen.

122
00:19:46,665 --> 00:19:49,665
Managed to wedge Gambit in between these bricks unscathed.

123
00:19:54,968 --> 00:19:57,968
Multiple cards can hit walls at the same time if done correctly.

124
00:20:04,436 --> 00:20:07,436
Dance Gambit, dance!

125
00:20:18,064 --> 00:20:21,064
Gambit can overlap an enemy so long as it's stunned and not take damage.

126
00:20:21,708 --> 00:20:24,708
Thanks to the blocks, we can reach the boss, the Black Queen, sooner than normal. This is the fastest route we could find.

127
00:20:25,036 --> 00:20:28,036
From there, we attack both of the boss' arms until they come off (18 hits each). The cards hit them as soon as they become vulnerable.

128
00:20:28,363 --> 00:20:31,363
After that, we attack the head 44 times and the battle is over.

129
00:20:31,691 --> 00:20:34,691
To see the full battle using a camera hack, see the submission page for a video.

130
00:20:41,642 --> 00:20:44,642
We can have Gambit hold his arm out without throwing cards because the explosions overload the screen with objects.

131
00:20:47,765 --> 00:20:50,765
Here are the final levels. Here, each character has a small segment to complete.

132
00:20:51,093 --> 00:20:54,093
These levels mostly employ a simple strategy: run right / left for great justice.

133
00:21:02,341 --> 00:21:05,341
We need to fall in between these barrels in order to pass.

134
00:21:05,669 --> 00:21:08,669
If we try to hit our head on the ceiling to descend faster, we'll just jump through it and hit it like a wall.

135
00:21:21,626 --> 00:21:24,626
We have to stall in order to get behind the first 2 clowns and kill them.

136
00:21:26,218 --> 00:21:29,218
We must jump over this clown to avoid getting hit, forcing us to stall at the ledge to continue.

137
00:21:32,541 --> 00:21:35,541
For some reason, jumping on the canister instead of falling onto it allows Wolverine to continue without stopping.

138
00:21:44,738 --> 00:21:47,738
Again, we use the fireball move to avoid unavoidable bullets.

139
00:22:01,211 --> 00:22:04,211
For some bizarre reason, jumping here is slower than falling. Oh well, we can still kill all of the enemies.

140
00:22:09,963 --> 00:22:12,963
As Storm plays like Gambit in her final level, Brandon primarily authors this section.

141
00:22:13,291 --> 00:22:16,291
She stalls when she jumps and has a special whirlwind move that operates like Gambit's fireball move.

142
00:22:16,618 --> 00:22:19,618
For some odd reason, the last projectile she fires at an enemy will go through it even though it hits.

143
00:22:19,946 --> 00:22:22,946
It's very hard to avoid taking damage in this level without wasting time.

144
00:22:23,274 --> 00:22:26,274
A lot of the optimization for this level is reducing lag. We did our best, but it could be improvable.

145
00:22:27,584 --> 00:22:30,584
We have Storm hit her head here to fall faster.

146
00:22:30,912 --> 00:22:33,912
The level ends when Storm lands, no matter how soon she reaches the goal. She gets pushed back for going beyond it.

147
00:22:50,013 --> 00:22:53,013
Here, we do a pixel skip damage boost like we did in the intro stage.

148
00:22:53,791 --> 00:22:56,791
We jump here instead of swinging so that we don't have to wait for the web to retract to make the next swing.

149
00:22:57,118 --> 00:23:00,118
Because of the low ceiling, it's impossible to do a web-swing as well as kill the thrower without wasting time.

150
00:23:00,446 --> 00:23:03,446
This section ends when Spider-Man's feet touch the bottom of the screen. Damage boosting and jumping takes the same amount of time.

151
00:23:03,758 --> 00:23:06,758
The final boss, Arcade, starts off in a large robot that operates like Russian nesting dolls with 24, 16, and 12HP respectively.

152
00:23:07,085 --> 00:23:10,085
This phase is similar to N'astirh in that swinging is useless and webshots deal 1 damage.

153
00:23:10,413 --> 00:23:13,413
After the smallest robot is destroyed, Arcade robots appear 1 by 1 until 3 appear at once. There are 10 total.

154
00:23:13,741 --> 00:23:16,741
Swinging kills them instantly, while web-shots and the X-Men need 2 hits. Gambit kills the second bot.

155
00:23:18,001 --> 00:23:21,001
Input can be cut off after starting this swing. Instead, we dance and press start on the next screen to reach the ending quicker.

156
00:23:21,162 --> 00:23:24,162
Thanks for watching this TAS of Spider-Man/X-Men: Arcade's Revenge by jaysmad and Brandon Evans!


# TAS

[Tool-Assisted Speedruns](https://tasvideos.org) and associated utilities.

## Emulator Versions
* NES: FCEUX 2.1.4a, FCEUX 2.1.5
* SNES: Snes9x rerecording 1.43 v17 svn123, Snes9x rerecording 1.51 v7 svn147